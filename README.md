# ChestBackpack #

ChestBackpack is an plugin for the Minecraft server software Spigot. It includes an Backpack for every player to your server, without any client mods. It is recommended to use a MySQL database for the plugin, but on small servers you can use the flatfile mode.
You need a special item to open your inventory unless you have special permissions. If you die an you have the item in your normal inventory, all item in the inventory will drop, this behavior can be disabled through permissions.

Special thanks to KadamaLee for the logo!

## Installation ##
First, download the latest version at the left side under the menu point "Downloads", unpack the file. Copy the .jar file into the plugins folder and start the Spigot server. That's all.
### Language ###
Now you can determine the language of the plugin. If you have your own language file, you have to copy it into the folder ChestBackpack in the plugins folder. In the config.yml now you have to insert the name of the file, if no file is indicated, by default english strings are used.
In the .zip-file with the plugin is also a default_lang.yml with english strings and a deutsch.yml with german strings.
### Database ###
It is recommended to use a MySQL database for saving data, because on servers with large number of different players the flatfile mode can slow down your server. To change the mode from flatfile to mysql mode you have to ways, the first way is for servers, where no players used the backpack before, the second method should be used, if you have already saved data in flatfile mode.
#### Clean system ####
If nobody has used the backpack before or all backpack data should be lost, thenn just open the file 'config.yml' in the ChestBackpack folder in the plugins folder. No insert your MySQL access data and change the node 'datamode' from 'flatfile' to 'mysql'. Now restart the server and you are done.

#### Running system ####
If you had already people that used the backpack then this way is the right one. This method migrates all backpack data from flatfile to MySQL database, and the best, you can do this on a running system. You can also migrate data, if players on the server, this players will be kicked before migration starts and no player can connect while migration is running.
Start your server, if not started jet. Then open the 'config.yml' file in the ChestBackpack folder in the plugins folder. Insert your MySQL access data, don't change the 'datamode' node!
Now insert the command 'backpack migrate' into the console, then confirm with 'backpack migrate ok'. Now migration is running. After migration is ready, you don't have to do anything else, everything is completed.
The old flatfile in the ChestBackpack folder will be saved with date and time of the migration in the same folder.

## Permissions ##

```
#!yaml
chestbackpack.lostInvAtDeath:
    description: If true you lost your backpack inventory at death if you have the backpack item in your inventory
    default: not op
chestbackpack.slots.9:
    description: Nine additional slots
chestbackpack.slots.18:
    description: Eightteen additional slots
chestbackpack.slots.27:
    description: Twentyseven additional slots
chestbackpack.slots.36:
    description: Thirtysix additional slots
chestbackpack.slots.45:
    description: Fourtyfive additional slots
chestbackpack.slots.54:
    description: Fityfour additional slots
    default: op
chestbackpack.command.open:
    description: Allows to open the backpack via the command, if false you get the packpack item through the command
    default: op
chestbackpack.command.other:
    description: Allows to access to the backpack of other players via command
    default: op

```

## Commands ##
/backpack - if you have "chestbackpack.command.open" permission, this command opens your backpack, if not, it gives you the backpack item.

/backpack item - Gives you the backpack item

/backpack player <player_name> - Opens the inventory of the player in read-only mode, only with permission "chestbackpack.command.other"

/backpack help - Shows all commands based on your permissions

/backpack ? - Same as /backpack help

## API ###
ChestBackpack includes since version 1.1.0 an easy to use API for developers with many methods to hook into ChestBackpack. A detailed description is comming some days later.

## Lizenz ##
[ ![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png) ](http://creativecommons.org/licenses/by-nc-sa/4.0/)

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)