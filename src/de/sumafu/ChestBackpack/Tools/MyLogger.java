package de.sumafu.ChestBackpack.Tools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

import org.bukkit.Bukkit;

import de.sumafu.ChestBackpack.main.ChestBackpack;

public class MyLogger {
	public MyLogger(ChestBackpack plugin) throws IOException{
		this.logFileDirectory = new File(plugin.getDataFolder()+System.getProperty("file.separator")+"log");
		if(!this.logFileDirectory.exists()){
			this.logFileDirectory.mkdir();
		}
		this.checkLogfile();
	}
	
	private File logFileDirectory = null;
	private File logFile = null;
	private FileWriter logWriter = null;
	private String currDateString = "";
	private final Lock lock = new ReentrantLock();
	
	private void checkLogfile() throws IOException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = sdf.format(new Date());
		
		if(this.currDateString.equals(dateString))return;
		
		this.currDateString = dateString;
		this.logFile = new File(this.logFileDirectory, this.currDateString+".log");
		this.logWriter = new FileWriter(logFile, true);
	}
	public void info(String message){
		this.log(Level.INFO, message);
	}
	
	public void log(Level level, String message){
		this.lock.lock();
		
		try {
			this.checkLogfile();
		} catch (IOException e1) {
			Bukkit.getPluginManager().getPlugin("ChestBackpack").getLogger().log(Level.WARNING, "Unable to load logfile!");
			e1.printStackTrace();
			this.lock.unlock();
			return;
		}
		
		SimpleDateFormat date_string = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentTime = new Date();
		StringBuilder builder = new StringBuilder();
		builder.append("["+date_string.format(currentTime)+" "+level.getName()+"]: ");
		builder.append(message);
		builder.append(System.getProperty("line.separator"));
		
		String logMessage = builder.toString();
		
		
		try {
			
			this.logWriter.write(logMessage);
			this.logWriter.flush();
			
		} catch (IOException e) {
			Bukkit.getPluginManager().getPlugin("ChestBackpack").getLogger().log(Level.SEVERE, "Unable to write to logfile: "+logMessage);
			e.printStackTrace();
		}
		
		this.lock.unlock();
		
	}
	
	public void closeLogFile() throws IOException{
		this.logWriter.close();
		this.currDateString="";
	}
}
