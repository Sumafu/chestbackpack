package de.sumafu.ChestBackpack.Data;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.sumafu.ChestBackpack.main.ChestBackpack;

public class FlatfileHandler {
	public FlatfileHandler(ChestBackpack plugin){
		this.plugin = plugin;
		
		try {
			this.reloadBackpackData();
			this.saveBackpackData();
		} catch (IOException e) {
			this.plugin.getLogger().log(Level.SEVERE, "Could not create file to save data", e);
			//e.printStackTrace();
			this.plugin.getServer().getPluginManager().disablePlugin(this.plugin);
		}
	}
	
	ChestBackpack plugin = null;
	
	private FileConfiguration backpackData = null;
	private File backpackFile = null;
	
	public void reloadBackpackData() {
		this.plugin.getLogger().info("reloadBackpackData()");
	    if (this.backpackFile == null) {
	    	this.backpackFile = new File(this.plugin.getDataFolder(), "BackpackData.yml");
	    }
	    this.backpackData = YamlConfiguration.loadConfiguration(backpackFile);
	}
	
	public FileConfiguration getHandler() {
	    if (this.backpackData == null) {
	    	this.reloadBackpackData();
	    }
	    return this.backpackData;
	}
	
	public void saveBackpackData() throws IOException {
	    if (this.backpackData == null || this.backpackFile == null) {
	        this.reloadBackpackData();
	    }
	    this.getHandler().save(this.backpackFile);
	}
}
