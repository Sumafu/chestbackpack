package de.sumafu.ChestBackpack.Tools;

/*
 * This plugin needs a default_lang.yml file in the jar file. This file includes the default strings.
 */
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Localize {
	private JavaPlugin plugin;
	private String languageFile = "default_lang.yml";
	
	public Localize(JavaPlugin plugin){
		this.plugin = plugin;
	}
	public Localize(JavaPlugin plugin, String langFile){
		this.plugin = plugin;
		this.setLanguageFile(langFile);
	}
	
	public void setLanguageFile(String langFile){
		if(langFile.equals("") || langFile == null){
			this.languageFile = "default_lang.yml";
		}else{
			this.languageFile = langFile;
		}
		this.reloadLocalizedStrings();
	}
	public String getLanguageFile(){
		return this.languageFile;
	}
	
	public String getLocalizedString(String pathToString){
		Object value = this.getLocalizedStrings().get(pathToString);
		if(value==null)return pathToString;
		else return (String)this.getLocalizedStrings().get(pathToString);
	}
	public void loadDefaultLanguageFiles(){
		String oldLanguageFile = this.getLanguageFile();
		
		this.setLanguageFile("default_lang.yml");
		this.getLocalizedStrings().options().copyDefaults(true);
		this.saveLocalizedStrings();
		
		this.setLanguageFile(oldLanguageFile);
		this.reloadLocalizedStrings();
	}
	
	private FileConfiguration localizedStrings = null;
	private File localizedStringsFile = null;
	
	private void reloadLocalizedStrings() {
	    localizedStringsFile = new File(plugin.getDataFolder(), languageFile);
	    localizedStrings = YamlConfiguration.loadConfiguration(localizedStringsFile);
	 
	    // Schaut nach den Standardwerten in der jar
	    InputStream defConfigStream = plugin.getResource("default_lang.yml");
	    if (defConfigStream != null) {
	    	Reader defConfigReader = new InputStreamReader(defConfigStream);
	        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigReader);
	        localizedStrings.setDefaults(defConfig);
	    }
	}
	private FileConfiguration getLocalizedStrings() {
	    if (localizedStrings == null) {
	        reloadLocalizedStrings();
	    }
	    return localizedStrings;
	}
	public void saveLocalizedStrings() {
	    if (localizedStrings == null || localizedStringsFile == null) {
	    return;
	    }
	    try {
	    	localizedStrings.save(localizedStringsFile);
	    } catch (IOException ex) {
	        Logger.getLogger(JavaPlugin.class.getName()).log(Level.SEVERE, "Could not save configuration to " + localizedStringsFile, ex);
	    }
	}
}