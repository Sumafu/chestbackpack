package de.sumafu.ChestBackpack.main;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import de.sumafu.ChestBackpack.Tools.Localize;

public class Backpack implements InventoryHolder{

	@Override
	public Inventory getInventory() {
		return null;
	}
	
	private boolean editable = true;
	
	static ItemStack getBackpack(Localize local){
		ItemStack item = new ItemStack(Material.CHEST, 1);
		ItemMeta meta = item.getItemMeta();
		
		meta.setDisplayName("§f"+local.getLocalizedString("main.backpack"));
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.DARK_PURPLE+"ChestBackpack");
		lore.add(ChatColor.DARK_PURPLE+local.getLocalizedString("main.placeChest"));
		meta.setLore(lore);
		
		try {
			// On Spigot-Server set item to unbreakable, on CraftBukkit-Server do nothing
			ItemMeta.class.getMethod("spigot", (Class<?>[]) null);
			
			meta.spigot().setUnbreakable(true);
		} catch (NoSuchMethodException | SecurityException e) {}
		
		item.setItemMeta(meta);
		
		return item;
	}
	static boolean isBackpack(ItemStack item){
		if(item==null)return false;
		if(!item.hasItemMeta())return false;
		if(!item.getItemMeta().hasLore())return false;
		if(item.getItemMeta().getLore().get(0).equals(ChatColor.DARK_PURPLE+"ChestBackpack")
				&& item.getType().equals(Material.CHEST)) return true;
		else return false;
	}
	
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	static boolean containsBackpackItem(PlayerInventory inv){
		for(ItemStack item : inv.getContents()){
			if(Backpack.isBackpack(item))return true;
		}
		return false;
	}
	
}
