package de.sumafu.ChestBackpack.main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import de.sumafu.ChestBackpack.Data.MigrationTool;
import de.sumafu.ChestBackpack.Tools.Localize;

public class CommandBackpack implements Runnable {
	public CommandBackpack(ChestBackpack chestBackpack, CommandSender sender, String[] args) {
		this.plugin = chestBackpack;
		this.sender = sender;
		this.args = args;
		this.local = this.plugin.localStrings;
		this.backpack = this.local.getLocalizedString("main.backpack");
	}
	ChestBackpack plugin = null;
	CommandSender sender = null;
	String[] args = null;
	Localize local = null;
	
	final String backpack;
	
	@Override
	public void run() {
		Player player = null;

		if(sender instanceof Player){
			player = (Player)sender;
		}else{
			if(args.length==1 && args[0].equals("migrate")){
				sender.sendMessage("Are you sure wanting to migrate data from local file to MySQL server?");
				sender.sendMessage("If you are sure, make sure that datamode in config.yml is set to 'flatfile' and the mysql access data are correct");
				sender.sendMessage("When you start migration, all players will be kicked and while migratin runs, nobody is able to join the server");
				sender.sendMessage("--------------------------------------");
				sender.sendMessage("Send command '/backpack migrate ok' within 60 seconds to start migration");
				this.plugin.lastMigrateInquiry = new Date();
			}else if(args.length==2 && args[0].equals("migrate") && args[1].equals("ok")){
				if(this.plugin.lastMigrateInquiry==null || ((new Date().getTime()/1000) - (this.plugin.lastMigrateInquiry.getTime()/1000)) > 60 ){
					sender.sendMessage("Type '/backpack migrate' first");
					return;
				}
				
				this.plugin.lastMigrateInquiry = null;
				this.plugin.getLogger().info("[Migration] Start migration");
				MigrationTool mTool = new MigrationTool(this.plugin);
				mTool.StartFlatfileToMySQLMigration();
			}else
				plugin.getLogger().info(local.getLocalizedString("error.notConsole"));
			return;
		}
		
		if(args.length==0){
			this.plugin.getMyLogger().info("Player "+player.getName()+" executed command '/backpack'");
			if(!PermissionsController.inventoryTroughCommand(player)){
				player.getInventory().addItem(Backpack.getBackpack(local));
				this.plugin.getMyLogger().info("Player "+player.getName()+" got backpack item");
			}else{
				this.plugin.openBackpack(player);
			}
		}else if(args.length==1){
			if(args[0].equals("item")){
				this.plugin.getMyLogger().info("Player "+player.getName()+" executed command '/backpack item'");
				player.getInventory().addItem(Backpack.getBackpack(local));
				this.plugin.getMyLogger().info("Player "+player.getName()+" got backpack item");
			}else if(args[0].equals("help") || args[0].equals("?")){
				this.plugin.getMyLogger().info("Player "+player.getName()+" executed command '/backpack help'");
				int slots = PermissionsController.numberOfSlots(player);
				List<String> message = new ArrayList<String>();
				message.add(ChatColor.AQUA+"-------- ChestBackpack "+local.getLocalizedString("help.help")+" --------");
				message.add("Your backpack has "+slots+" slots");
				if(PermissionsController.inventoryTroughCommand(player)){
					message.add(ChatColor.YELLOW+"/backpack "+ChatColor.GREEN+"- "+local.getLocalizedString("help.open"));
					message.add(ChatColor.YELLOW+"/backpack item "+ChatColor.GREEN+"- "+local.getLocalizedString("help.get"));
				}else{
					message.add(ChatColor.YELLOW+"/backpack "+ChatColor.GREEN+"- "+local.getLocalizedString("help.get"));
				}
				if(PermissionsController.getInvOfOther(player)){
					message.add(ChatColor.YELLOW+"/backpack player <name> "+ChatColor.GREEN+"- "+local.getLocalizedString("help.openOther"));
				}
				if(PermissionsController.dropInventoryThroughDeath(player)){
					message.add(ChatColor.GOLD+local.getLocalizedString("help.dieAndLost"));
				}
				String[] messages = new String[message.size()];
				messages = message.toArray(messages);
				player.sendMessage(messages);
			}
		}else if(args.length==2){
			if(args[0].equals("player")){
				if(!PermissionsController.getInvOfOther(player)){
					player.sendMessage(ChatColor.RED+local.getLocalizedString("error.noPermissions"));
					return;
				}
				this.plugin.getMyLogger().info("Player "+player.getName()+" executed command '/backpack player "+args[1]+"'");
				this.plugin.getServer().getScheduler().runTaskAsynchronously(this.plugin, new MyRunnable(player, args[1]){
					@Override
					public void run() {
						Player player = (Player) params[0];
						String invPlayer = (String) params[1];
						
						player.sendMessage(local.getLocalizedString("main.isOpening").replace("{%player%}", invPlayer));
						
					
						OfflinePlayer[] offPlayer = Bukkit.getOfflinePlayers();
						
						boolean hasPlayedBefor = false;
						UUID uuid = null;
						for(OfflinePlayer plyr: offPlayer){
							if(plyr.getName().equals(invPlayer)){
								hasPlayedBefor = true;
								uuid = plyr.getUniqueId();
								break;
							}
						}
						if(hasPlayedBefor){
							
							try {
								Inventory inv = ChestBackpack.getAPI().getInventory(uuid);
								
								if(inv==null)
									player.sendMessage(local.getLocalizedString("error.noInv"));
								else{
									((Backpack)inv.getHolder()).setEditable(false);
									player.openInventory(inv);
								}
								
							} catch (CannotLoadBackpackException e1) {
								player.sendMessage(local.getLocalizedString("error.loadBackpack"));
								return;
							}
						}else{
							sender.sendMessage(ChatColor.RED+local.getLocalizedString("error.playerNotExist"));
						}
						
					}
				});
			}
		}
	}
}
