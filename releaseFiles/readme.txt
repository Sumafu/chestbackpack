******************************************************************
                     ChestBackpack ReadMe
******************************************************************

1. Installation

First, download the latest version at the left side under the menu point "Downloads", unpack the
file. Copy the .jar file into the plugins folder and start the Spigot server. That's all.

2. Database

It is recommended to use a MySQL database for saving data, because on servers with large number
of different players the flatfile mode can slow down your server. To change the mode from flatfile
to mysql mode you have to ways, the first way is for servers, where no players used the backpack before,
the second method should be used, if you have already saved data in flatfile mode.

2.1 Clean system

If nobody has used the backpack before or all backpack data should be lost, thenn just open the file
'config.yml' in the ChestBackpack folder in the plugins folder. No insert your MySQL access data and
change the node 'datamode' from 'flatfile' to 'mysql'. Now restart the server and you are done.

2.2 Running system

If you had already people that used the backpack then this way is the right one. This method migrates
all backpack data from flatfile to MySQL database, and the best, you can do this on a running system.
You can also migrate data, if players on the server, this players will be kicked before migration starts
and no player can connect while migration is running.
Start your server, if not started jet. Then open the 'config.yml' file in the ChestBackpack folder in
the plugins folder. Insert your MySQL access data, don't change the 'datamode' node!
Now insert the command 'backpack migrate' into the console, then confirm with 'backpack migrate ok'.
Now migration is running. After migration is ready, you don't have to do anything else, everything
is completed.
The old flatfile in the ChestBackpack folder will be saved with date and time of the migration in
the same folder.


3. Language

You can determine the language of the plugin. As default the plugin is in english,
therefore you have nothing to do. In this folder you find a file "german.yml" with german
strings, and you find a "default_lang.yml" with the default english strings. this file is
for self translating.
If you have a language file that you want to use, you have to copy it in the folder
ChestBackpack in the plugins folder. Then open the "config.yml" and insert the name of
the language file in the node "languge_file" and restart your server.

4. Permissions

Here is a list with all permissions:

chestbackpack.lostInvAtDeath:
    description: If true you lost your backpack inventory at death if you have the backpack item in your inventory
    default: not op
chestbackpack.slots.9:
    description: Nine additional slots
chestbackpack.slots.18:
    description: Eightteen additional slots
chestbackpack.slots.27:
    description: Twentyseven additional slots
chestbackpack.slots.36:
    description: Thirtysix additional slots
chestbackpack.slots.45:
    description: Fourtyfive additional slots
chestbackpack.slots.54:
    description: Fityfour additional slots
    default: op
chestbackpack.command.open:
    description: Allows to open the backpack via the command, if false you get the packpack item through the command
    default: op
chestbackpack.command.other:
    description: Allows to access to the backpack of other players via command
    default: op

5. Commands

/backpack - if you have "chestbackpack.command.open" permission, this command opens your backpack, if not, it gives you the backpack item.
/backpack item - Gives you the backpack item
/backpack player <player_name> - Opens the inventory of the player in read-only mode, only with permission "chestbackpack.command.other"
/backpack help - Shows all commands based on your permissions
/backpack ? - Same as /backpack help

6. API

ChestBackpack includes an easy to use API for developers with many methods to hook into ChestBackpack. For more
detailes visit the Bitbucket page of ChestBackpack: www.bitbucket.org/Sumafu/chestbackpack