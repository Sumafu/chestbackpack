package de.sumafu.ChestBackpack.main;

import org.bukkit.entity.Player;

public class PermissionsController {
	public PermissionsController(ChestBackpack plugin){
		this.plugin = plugin;
	}
	
	ChestBackpack plugin = null;
	
	static int numberOfSlots(Player player){
		
		if(player.hasPermission("chestbackpack.slots.54"))return 54;
		if(player.hasPermission("chestbackpack.slots.45"))return 45;
		if(player.hasPermission("chestbackpack.slots.36"))return 36;
		if(player.hasPermission("chestbackpack.slots.27"))return 27;
		if(player.hasPermission("chestbackpack.slots.18"))return 18;
		if(player.hasPermission("chestbackpack.slots.9"))return 9;
		
		return 0;
	}
	
	static boolean inventoryTroughCommand(Player player){
		if(player.hasPermission("chestbackpack.command.open"))return true;
		return false;
	}
	
	static boolean dropInventoryThroughDeath(Player player){
		if(player.hasPermission("chestbackpack.lostInvAtDeath"))return true;
		return false;
	}
	
	static boolean getInvOfOther(Player player){
		if(player.hasPermission("chestbackpack.command.other"))return true;
		return false;
	}
}
