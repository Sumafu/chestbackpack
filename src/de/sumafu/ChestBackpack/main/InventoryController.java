package de.sumafu.ChestBackpack.main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import de.sumafu.ChestBackpack.Tools.InventoryStringDeSerializer;

public class InventoryController implements Listener {
	public InventoryController(ChestBackpack plugin){
		this.plugin = plugin;
		
		this.flusher = new MyRunnable(this.plugin){
			@Override
			public void run() {
				
				UUID onlyUUID = (UUID) this.data.get("onlyUUID");
				Inventory onlyInv = (Inventory) this.data.get("onlyUuidInventory");
				
				lock.lock();
				
				ChestBackpack plugin = (ChestBackpack) params[0];
				
				Map<UUID, String> inventorys = null;
				String saveDataMessage = "";
				
				if(onlyUUID==null){
					try {
						inventorys = ChestBackpack.getAPI().getInventoryStrings(null);
					} catch (IOException e) {
						e.printStackTrace();
						lock.unlock();
						return;
					}
					if(plugin.getConfig().getBoolean("saveSucceedMessageInConsole"))
						saveDataMessage = "Try to save data...";
				}
				else{
					String inv = null;
					try {
						inv = InventoryStringDeSerializer.InventoryToString(onlyInv);
					} catch (IOException e) {
						e.printStackTrace();
						lock.unlock();
						return;
					}
					
					inventorys = new HashMap<UUID, String>();
					inventorys.put(onlyUUID, inv);
					saveDataMessage="Try to save backpack of "+onlyUUID.toString();
				}
				
				if(!saveDataMessage.equals(""))
					plugin.getLogger().info(saveDataMessage);
				
				boolean saveData=true;
				for(Entry<UUID, String> entry : inventorys.entrySet()){
					String uuid = entry.getKey().toString();
					String invString = entry.getValue();
					
					try {
						plugin.dataHandler.saveBackpack(UUID.fromString(uuid), invString);
					} catch (SQLException | IOException e) {
						plugin.getLogger().log(Level.WARNING, "Failed to save backpack of "+uuid+", try to save second time...");
						
						try{
							plugin.dataHandler.saveBackpack(UUID.fromString(uuid), invString);
						}catch (SQLException | IOException e1){
							plugin.getLogger().log(Level.WARNING, "Failed to save backpack of "+uuid);
							plugin.getMyLogger().log(Level.SEVERE, "Failed to save backpack of "+uuid+" : "+invString);
							e.printStackTrace();
							saveData=false;
						}
					}
					
				}
				if(saveData){
					if(plugin.getConfig().getBoolean("saveSucceedMessageInConsole"))
						plugin.getLogger().info("Data saved successfully");
				}else{
					plugin.getLogger().log(Level.SEVERE, "Failed to save some data, show ChestBackpack log for more information");
				}
				
				lock.unlock();
			}
		};
	}
	
	private ChestBackpack plugin = null;
	public MyRunnable flusher = null;
	
	final Lock lock = new ReentrantLock();
	
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent event){
		this.lock.lock();
		
		InventoryHolder holder = event.getInventory().getHolder();
		
		if(holder instanceof Backpack){
			Backpack bp = (Backpack) holder;
			if(!bp.isEditable()){
				bp.setEditable(true);
				this.lock.unlock();
				return;
			}
			
			Inventory inv = event.getInventory();
			UUID uuid = event.getPlayer().getUniqueId();
			
			ChestBackpack.getAPI().setInventory(uuid, inv);
			this.plugin.getMyLogger().info(event.getPlayer().getName()+" closed backpack");
		
			this.plugin.openBackpacks.remove(uuid);
		}
		this.lock.unlock();
	}
	
	@EventHandler
	public void onBlock(BlockPlaceEvent event){
		this.lock.lock();
		Player player = event.getPlayer();
		
		if(Backpack.isBackpack(event.getItemInHand())){
			event.setCancelled(true);
			this.plugin.getMyLogger().info("Player "+player.getName()+" placed backpack item");
			
			this.plugin.openBackpack(player);
			
		}
		
		this.lock.unlock();
		
	}
	
	@EventHandler
    public void onInventoryClick(InventoryClickEvent event){
		if(event.getInventory().getHolder() instanceof Backpack){
			if(!((Backpack)event.getInventory().getHolder()).isEditable()){
				event.setCancelled(true);
			}
		}
	}
	
	public void flushData(UUID uuid){
		this.lock.lock();
		
		this.flusher.data.put("onlyUUID", uuid);
		this.flusher.data.put("onlyUuidInventory", ChestBackpack.getAPI().intGetInventory(uuid));
		this.plugin.getServer().getScheduler().runTaskAsynchronously(this.plugin, flusher);
		
		this.lock.unlock();
	}
	
	void loadData(UUID uuid){
		this.loadData(uuid, 0);
	}
	
	void loadData(UUID uuid, int warmup){
		this.lock.lock();
		
		MyRunnable thread = new MyRunnable(this.plugin, uuid, warmup){
			@Override
			public void run(){
				ChestBackpack plugin = (ChestBackpack) params[0];
				UUID uuid = (UUID) params[1];
				Player player = Bukkit.getPlayer(uuid);
				int warmup = (int) params[2];
				
				plugin.warmup.put(uuid, true);
				try {
					Thread.sleep(warmup*1000);
				} catch (InterruptedException e1) {}
				plugin.warmup.remove(uuid);
				if(!player.isOnline())return;
				
				try{
					if(!plugin.dataHandler.loadBackpack(uuid)){
						int numberofSlots = PermissionsController.numberOfSlots(player);
						Inventory emptyInv = ChestBackpack.getAPI().getEmptyInventory(numberofSlots);
						ChestBackpack.getAPI().setInventory(uuid, emptyInv);
					}
					if(plugin.getConfig().getBoolean("printLoginMessage"))
						player.sendMessage(plugin.localStrings.getLocalizedString("main.loaded"));
					plugin.getMyLogger().info("Backpack of "+player.getName()+" loaded");
					
				} catch (IllegalArgumentException | SQLException | ClassNotFoundException | IOException e) {
					player.sendMessage(ChatColor.RED+plugin.localStrings.getLocalizedString("error.loadBackpack"));
					plugin.getMyLogger().log(Level.SEVERE, "Failed to load backpack of "+player.getName()+". Show serverlog for detailed error.");
					e.printStackTrace();
				}
			}
		};
		this.plugin.getServer().getScheduler().runTaskAsynchronously(this.plugin, thread);
		
		this.lock.unlock();
		
	}
	void dropAllItems(Player player){
		this.lock.lock();
		UUID uuid = player.getUniqueId();
		Inventory inv = ChestBackpack.getAPI().intGetInventory(uuid);
		
		if(inv==null){
			this.lock.unlock();
			return;
		}
		
		for(ItemStack is : inv.getContents()){
			if(is!=null){
				player.getWorld().dropItemNaturally(player.getLocation(), is);
		        player.getInventory().remove(is);
			}
		}
		
		int numberofSlots = PermissionsController.numberOfSlots(player);
		Inventory emptyInv = ChestBackpack.getAPI().getEmptyInventory(numberofSlots);
		ChestBackpack.getAPI().setInventory(uuid, emptyInv);
		
		this.lock.unlock();
	}
	void playerDisconnect(Player player){
		
	}
}
