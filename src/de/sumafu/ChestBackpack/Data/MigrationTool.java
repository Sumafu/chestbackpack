package de.sumafu.ChestBackpack.Data;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import de.sumafu.ChestBackpack.main.*;

public class MigrationTool {
	public MigrationTool(ChestBackpack plugin){
		this.plugin = plugin;
	}
	
	ChestBackpack plugin = null;
	
	public boolean StartFlatfileToMySQLMigration(){
		this.plugin.MigrationMode = true;
		this.plugin.getLogger().info("[Migration] Set migration mode true, no players can connect more, wait 5 seconds for global kick");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {}
		this.plugin.getLogger().info("[Migration] Kick all players on server");
		
		Future<String> future = Bukkit.getServer().getScheduler().callSyncMethod(this.plugin, new Callable<String>() {

			@Override
			public String call() throws Exception {
				
				ChestBackpack plugin = (ChestBackpack) Bukkit.getServer().getPluginManager().getPlugin("ChestBackpack");

				for(Player player : Bukkit.getServer().getOnlinePlayers()){
					player.kickPlayer(plugin.localStrings.getLocalizedString("other.migrateKick"));
				}
				
				return null;
			}
		});
		
		while(!future.isDone()){
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}
		
		
		this.plugin.getLogger().info("[Migration] All players on server kicked");
		this.plugin.getLogger().info("[Migration] Flush all data to file and clear local list");
		
		this.plugin.invControl.flusher.data.put("onlyUUID", null);
		this.plugin.invControl.flusher.data.put("onlyUuidInventory", null);
		this.plugin.invControl.flusher.run();
		
		this.plugin.clearInventorys();
		this.plugin.getLogger().info("[Migration] Start data migration");
		this.plugin.getLogger().info("[Migration] Load all backpack data");
		
		try {
			this.plugin.dataHandler.loadBackpack();
		} catch (SQLException | ClassNotFoundException | IOException e) {} // In flatfile mode this error never will throw
		
		this.plugin.getLogger().info("[Migration] Set datamode to 'mysql'");
		this.plugin.getConfig().set("datamode", "mysql");
		this.plugin.saveConfig();
		
		this.plugin.getLogger().info("[Migration] Initiante new DataHandler");
		this.plugin.dataHandler = new DataHandler(this.plugin);
		if(!this.plugin.dataHandler.getMySQLReady()){
			this.plugin.getLogger().log(Level.SEVERE, "[Migration] Failed to connect to MySQL server, try a second time");
			
			this.plugin.dataHandler = new DataHandler(this.plugin);
			if(!this.plugin.dataHandler.getMySQLReady()){
				this.plugin.getLogger().log(Level.SEVERE, "[Migration] Failed to connect to MySQL server!");
				this.plugin.getLogger().info("[Migration] Migration of data failed, recreate old configuration");
				
				this.plugin.clearInventorys();
				this.plugin.getConfig().set("datamode", "flatfile");
				this.plugin.saveConfig();
				this.plugin.dataHandler = new DataHandler(this.plugin);
				
				this.plugin.getLogger().info("[Migration] Old configuration recreated");
				this.plugin.MigrationMode = false;
				this.plugin.getLogger().info("[Migration] Set migration mode false, players can connect again");
				this.plugin.getLogger().info("[Migration] Migration finished");
				return false;
			}
		}
		
		this.plugin.getLogger().info("[Migration] Write data to database");
		this.plugin.invControl.flusher.data.put("onlyUUID", null);
		this.plugin.invControl.flusher.data.put("onlyUuidInventory", null);
		this.plugin.invControl.flusher.run();
		this.plugin.getLogger().info("[Migration] Writing has finished");
		
		this.plugin.getLogger().info("[Migration] Clear local data list");
		this.plugin.clearInventorys();
		
		this.plugin.getLogger().info("[Migration] Rename old data file");
		String dateTime = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(new Date());
		File oldFile = new File(this.plugin.getDataFolder(), "BackpackData.yml");
		File newFile = new File(this.plugin.getDataFolder(), "BackpackData_migrated_"+dateTime+".yml");
		
		if(!oldFile.renameTo(newFile))
			this.plugin.getLogger().log(Level.WARNING, "[Migration] Failed to rename file");
		
		this.plugin.getLogger().info("[Migration] Set migration mode false, players can connect again");
		this.plugin.MigrationMode = false;
		
		this.plugin.getLogger().info("[Migration] Migration finished");
		return true;
	}
}
