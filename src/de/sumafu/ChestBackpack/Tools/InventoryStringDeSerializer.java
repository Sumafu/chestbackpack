package de.sumafu.ChestBackpack.Tools;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
 
public class InventoryStringDeSerializer {
	public static String InventoryToString (Inventory invInventory) throws IOException
    {
		if(invInventory==null)return "";
		
        String serialization = "v2@"+invInventory.getSize() + ";";
        for (int i = 0; i < invInventory.getSize(); i++)
        {
            ItemStack is = invInventory.getItem(i);
            if (is != null)
            {
            	ByteArrayOutputStream os = new ByteArrayOutputStream();
				BukkitObjectOutputStream bos = new BukkitObjectOutputStream(os);
            	
				bos.writeObject(is);
				
				byte[] k = null;
				k = os.toByteArray();
				
				String data = javax.xml.bind.DatatypeConverter.printBase64Binary(k);
				
				//System.out.println("w: "+data);
				
				serialization += i + "@" + mask(data) + ";";
				
				bos.close();
            	
            }
        }
        return serialization;
    }
   
	public static Inventory StringToInventory (String invString, InventoryHolder player, int numberOfSlots, String title) throws IOException, ClassNotFoundException
    {
		if(numberOfSlots==0) return null;
		
        String[] serializedBlocks = invString.split(";");
        Inventory deserializedInventory = Bukkit.getServer().createInventory(player, numberOfSlots, title);
       
        if(serializedBlocks[0].split("@")[0].equals("v2")){
        	
        	for (int i = 1; i < serializedBlocks.length; i++){
        		String[] serializedBlock = serializedBlocks[i].split("@");
        		
        		int position = Integer.valueOf(serializedBlock[0]);
        		if (position >= deserializedInventory.getSize()) continue;
        		
        		//System.out.println("r: "+demask(serializedBlock[1]));
        		byte[] data = javax.xml.bind.DatatypeConverter.parseBase64Binary(demask(serializedBlock[1]));
        		
        		InputStream in = new ByteArrayInputStream(data);
        		BukkitObjectInputStream bis = new BukkitObjectInputStream(in);
        		
        		ItemStack is = (ItemStack) bis.readObject();
        		
        		deserializedInventory.setItem(position, is);
        		
        		bis.close();
        	}
        	
        }
        else{ // v1
	        for (int i = 1; i < serializedBlocks.length; i++){
	            String[] serializedBlock = serializedBlocks[i].split("#");
	            int stackPosition = Integer.valueOf(serializedBlock[0]);
	            if (stackPosition >= deserializedInventory.getSize()) continue;
	            ItemStack is = null;
	            Boolean createdItemStack = false;
	            String[] serializedItemStack = serializedBlock[1].split(":");
	            for (String itemInfo : serializedItemStack){
	                String[] itemAttribute = itemInfo.split("@");
	                if (itemAttribute[0].equals("t")){
	                    is = new ItemStack(Material.getMaterial(itemAttribute[1]));
	                    createdItemStack = true;
	                }else if (itemAttribute[0].equals("d") && createdItemStack){
	                    is.setDurability(Short.valueOf(itemAttribute[1]));
	                }else if (itemAttribute[0].equals("a") && createdItemStack){
	                    is.setAmount(Integer.valueOf(itemAttribute[1]));
	                }else if (itemAttribute[0].equals("e") && createdItemStack){
	                    is.addEnchantment(Enchantment.getByName(itemAttribute[1]), Integer.valueOf(itemAttribute[2]));
	                }else if (itemAttribute[0].equals("l") && createdItemStack){
	                	ItemMeta im = is.getItemMeta();
	                	if(im.hasLore()){
	                		List<String> lore = im.getLore();
	                		lore.add(itemAttribute[1]);
	                		im.setLore(lore);
	                	}else{
	                		List<String> lore = new ArrayList<String>();
	                		lore.add(itemAttribute[1]);
	                		im.setLore(lore);
	                	}
	                	is.setItemMeta(im);
	                }else if (itemAttribute[0].equals("dn") && createdItemStack){
	                	ItemMeta im = is.getItemMeta();
	                	im.setDisplayName(itemAttribute[1]);
	                	is.setItemMeta(im);
	                }
	            }
	            deserializedInventory.setItem(stackPosition, is);
	        } // v1 end
        } 
       
        return deserializedInventory;
    }
	
	public static String mask(String orig){
		String ret = orig;
		ret = ret.replace("&", "&38$");
		ret = ret.replace("@", "&64$");
		ret = ret.replace("=", "&61$");
		ret = ret.replace(";", "&59$");
		return ret;
	}
	public static String demask(String orig){
		String ret = orig;
		ret = ret.replace("&59$", ";");
		ret = ret.replace("&61$", "=");
		ret = ret.replace("&64$", "@");
		ret = ret.replace("&38$", "&");
		return ret;
	}
}
 