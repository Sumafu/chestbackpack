package de.sumafu.ChestBackpack.Data;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;

import de.sumafu.ChestBackpack.Tools.InventoryStringDeSerializer;
import de.sumafu.ChestBackpack.main.Backpack;
import de.sumafu.ChestBackpack.main.ChestBackpack;

/**
 * Handles every construct to save or load data. In this class you can add your own methods to save data.
 * Use switch-case constructs to implement own methods
 * 
 * @author Sumafu
 */
public class DataHandler {
	public DataHandler(ChestBackpack plugin){
		this.plugin = plugin;
		
		switch(this.plugin.getConfig().getString("datamode")){
		case "mysql":
			this.mode = md_mysql;
			
			this.connector = new DatabaseConnector(this.plugin);
			this.mysqlReady = connector.connectMySQL();
			break;
		default:
			this.mode = md_flatfile;
			
			this.filehandler = new FlatfileHandler(this.plugin);
			
			
		}
	}
	
	// Constants to differ between different data modes
	static final int md_flatfile = 0;
	static final int md_mysql = 1;
	
	private ChestBackpack plugin;
	private int mode = md_flatfile;
	
	private boolean mysqlReady = false;
	private DatabaseConnector connector = null;
	private FlatfileHandler filehandler = null;
	
	/**
	 * Only useable with "mysql" mode
	 * 
	 * @return true if mysql connection is ready to use<br>false if no mysql connection is available
	 */
	public boolean getMySQLReady(){
		return mysqlReady;
	}
	
	/**
	 * Save backpack of specified player
	 * 
	 * @param uuid UUID of player whose backpack should be saved
	 * @param inv Inventory to save
	 * @return true if saving succeeded<br>false if saving run into error
	 * @throws SQLException only in "mysql" mode
	 * @throws IOException only in "flatfile" mode
	 * @see DataHandler#saveBackpack(UUID, String)
	 */
	public boolean saveBackpack(UUID uuid, Inventory inv) throws SQLException, IOException{
		return this.saveBackpack(uuid, InventoryStringDeSerializer.InventoryToString(inv));
	}
	
	/**
	 * Save backpack of specified player
	 * 
	 * @param uuid UUID of player whose backpack should be saved
	 * @param invString Backpack as serialized String, see {@link InventoryStringDeSerializer}
	 * @return true if saving succeeded<br>false if saving run into error
	 * @throws SQLException only in "mysql" mode
	 * @throws IOException only in "flatfile" mode
	 */
	public boolean saveBackpack(UUID uuid, String invString) throws SQLException, IOException{
		switch(this.mode){
		case md_mysql:
			this.connector.reActivateMySQLConnection();
			
			Statement stmt = this.connector.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS `Anzahl` FROM `de.sumafu.ChestBackpack.Backpack` WHERE BpUUID = '"+uuid+"'");
			rs.next();
			if(rs.getInt("Anzahl")==0){
				PreparedStatement ps = this.connector.getConnection().prepareStatement("INSERT INTO `de.sumafu.ChestBackpack.Backpack` (`BpUUID`, `BpInvString`) VALUES (?,?)");
				ps.setString(1, uuid.toString());
				ps.setString(2, invString);
				ps.executeUpdate();
			}else{
				if(invString==""){
					PreparedStatement ps = this.connector.getConnection().prepareStatement("DELETE FROM `de.sumafu.ChestBackpack.Backpack` WHERE `BpUUID`=?");
					ps.setString(1, uuid.toString());
					ps.executeUpdate();
				}else{
					PreparedStatement ps = this.connector.getConnection().prepareStatement("UPDATE `de.sumafu.ChestBackpack.Backpack` SET `BpInvString`=? WHERE `BpUUID`=?");
					ps.setString(1, invString);
					ps.setString(2, uuid.toString());
					ps.executeUpdate();
				}
			}
			
			
			return true;
		case md_flatfile:
			if(invString=="") return true;
				
			this.filehandler.getHandler().set(uuid.toString(), invString);
			this.filehandler.saveBackpackData();
			
			return true;
		default:
			return false;
		}
	}
	
	/**
	 * Load every backpack found in database. It is not recommend to use this method on servers with a large amount of players.
	 * 
	 * @return true if loading succeeded<br>false if loading run into error
	 * @throws SQLException only in "mysql" mode
	 * @throws IOException Error while deserializing inventory
	 * @throws ClassNotFoundException Error while deserializing inventory 
	 */
	public boolean loadBackpack() throws SQLException, ClassNotFoundException, IOException{
		switch(this.mode){
		case md_mysql:
			Statement stmt = this.connector.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM `de.sumafu.ChestBackpack.Backpack`");
			while(rs.next()){
				this.saveBackpackToInternalList(UUID.fromString(rs.getString("BpUUID")), rs.getString("BpInvString"));
				
			}
			return true;
		case md_flatfile:
			
			Set<String> uuids = this.filehandler.getHandler().getKeys(false);
			
			for(String uuid : uuids){
				this.saveBackpackToInternalList(UUID.fromString(uuid), this.filehandler.getHandler().getString(uuid));
			}
			
			return true;
		default:
			return false;
		}
	}
	
	/**
	 * Load only the backpack of player with given uuid to local storage
	 * 
	 * @param uuid UUID of player, whose backpack should be loaded
	 * @return	true if loading succeeded<br>false if loading run into error
	 * @throws SQLException only in "mysql" mode
	 * @throws IOException Error while deserializing inventory
	 * @throws ClassNotFoundException Error while deserializing inventory
	 */
	public boolean loadBackpack(UUID uuid) throws SQLException, ClassNotFoundException, IOException{
		switch(this.mode){
		case md_mysql:
			this.connector.reActivateMySQLConnection();
			
			Statement stmt = this.connector.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM `de.sumafu.ChestBackpack.Backpack` WHERE `BpUUID` = '"+uuid.toString()+"'");
			String inv;
			if(!rs.last())
				inv = "";
			else
				inv = rs.getString("BpInvString");
			this.saveBackpackToInternalList(uuid, inv);
			return true;
		case md_flatfile:
			String backpack = this.filehandler.getHandler().getString(uuid.toString());
			if(backpack==null)backpack="";
			
			this.saveBackpackToInternalList(uuid, backpack);
			
			return true;
		default:
			return false;
		}
	}
	
	private void saveBackpackToInternalList(UUID uuid, String invString) throws ClassNotFoundException, IOException{
		String title = this.plugin.localStrings.getLocalizedString("main.backpack");
		
		int slots = 0;
		if(Bukkit.getOfflinePlayer(uuid).isOnline())
			slots = this.plugin.getInventorySize(uuid);
		else{
			String count = invString.split(";")[0].split("@")[1];
			slots = Integer.parseInt(count);
		}
		
		Backpack holder = new Backpack();
		
		Inventory inv;
		if(invString==""||invString==null){
			inv = ChestBackpack.getAPI().getEmptyInventory(slots);
		}else{
			inv = InventoryStringDeSerializer.StringToInventory(invString, holder, slots, title);
		}
		
		ChestBackpack.getAPI().setInventory(uuid, inv);
	}
}
