package de.sumafu.ChestBackpack.main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import de.sumafu.ChestBackpack.Tools.InventoryStringDeSerializer;

/**
 * With this methods you can hook into this plugin with your own plugin. Notice that you should use this methods only in a seperated Thread
 * to prevent server freeze
 * 
 * @author Sumafu
 *
 */
public class ChestBackpackAPI {
	public ChestBackpackAPI(ChestBackpack plugin){
		this.plugin = plugin;
	}
	
	private ChestBackpack plugin = null;
	private final Lock lock = new ReentrantLock();
	private Map<UUID, Inventory> inventare = new HashMap<UUID, Inventory>();
	
	private void log(Level level, String message){
		this.plugin.getMyLogger().log(level, "[API] "+message);
	}
	private void info(String message){
		this.log(Level.INFO, message);
	}
	private void error(String message){
		this.log(Level.SEVERE, message);
	}
	
	void initBackpack(UUID uuid){
		
	}
	
	Inventory intGetInventory(UUID uuid){
		return this.inventare.get(uuid);
	}
	void intSetInventory(UUID uuid, Inventory inventory){
		this.lock.lock();
		this.inventare.put(uuid, inventory);
		this.lock.unlock();
	}
	void intRemoveInventory(UUID uuid){
		this.lock.lock();
		this.inventare.remove(uuid);
		this.lock.unlock();
	}
	boolean intContainsKey(UUID uuid){
		return this.inventare.containsKey(uuid);
	}
	void intClear(){
		this.lock.lock();
		this.inventare.clear();
		this.lock.unlock();
	}
	
	/**
	 * Returns the current inventory of the player
	 * 
	 * @param uuid UUID of the player
	 * @return The inventory of the player 
	 * @throws CannotLoadBackpackException Throw if backpack cannot be loaded
	 */
	public Inventory getInventory(UUID uuid) throws CannotLoadBackpackException{
		return this.getInventory(uuid, true);
	}
	
	private Inventory getInventory(UUID uuid, boolean locking) throws CannotLoadBackpackException{
		try {
			this.info("Try to get inventory of "+uuid.toString());
			if(locking) lock.lock();
			if(!this.inventare.containsKey(uuid)){
				this.plugin.dataHandler.loadBackpack(uuid);
			}
			Inventory inventory = this.inventare.get(uuid);
			if(locking) lock.unlock();
			return inventory;
		} catch (SQLException | ClassNotFoundException | IOException e) {
			this.error("Cannot load backpack of "+uuid.toString()+". "+e.getMessage());
			if(locking) lock.unlock();
			throw new CannotLoadBackpackException(uuid, e);
		}
	}

	public Inventory getEmptyInventory(int slots) {
		if(slots==0) return null;
		
		String title = this.plugin.localStrings.getLocalizedString("main.backpack");
		Backpack bp = new Backpack();

		return Bukkit.createInventory(bp, slots, title);
	}

	/**
	 * Sets the inventory to the player. Note that this method is not secure to use, it is possible that your changes can be
	 * overwritten or you overwrite changes of player.
	 * 
	 * @param uuid UUID of the palyer
	 * @param inventory Inventory to set 
	 */
	public void setInventory(UUID uuid, Inventory inventory){
		this.setInventory(uuid, inventory, true);
	}
	private void setInventory(UUID uuid, Inventory inventory, boolean setLock){
		if(setLock)lock.lock();		
		this.inventare.put(uuid, inventory);
		if(setLock)lock.unlock();
	}
	
	/**
	 * Opens the inventory of the player
	 * 
	 * @param uuid UUID of the player
	 * @return  0 - Backpack opened successfully<br>
	 * 			1 - Cannot opened, player is offline<br>
	 * 			2 - Cannot opened, player has no rights<br>
	 * 			3 - Cannot opened, player is in warmup<br>
	 * 			4 - Cannot opened, is already open
	 * @throws CannotLoadBackpackException Throw if backpack cannot be loaded
	 */
	public int openBackpack(UUID uuid) throws CannotLoadBackpackException{
		lock.lock();
		this.info("Try to open backpack of "+uuid.toString());
		if(!Bukkit.getOfflinePlayer(uuid).isOnline()){
			this.log(Level.WARNING, "Cannot open backpack of "+uuid.toString()+", because player is offline");
			lock.unlock();	
			return 1;
		}
		if(this.backpackSize(uuid)==0){
			this.log(Level.WARNING, "Cannot open backpack of "+uuid.toString()+", because he has no rights to use backpack");
			lock.unlock();
			return 2;
		}
		if(this.plugin.warmup.containsKey(uuid)){
			this.log(Level.WARNING, "Cannot open backpack of "+uuid.toString()+", because he is in warmup");
			lock.unlock();
			return 3;
		}
		if(this.plugin.openBackpacks.containsKey(uuid)){
			this.log(Level.WARNING, "Cannot open backpack of "+uuid.toString()+", because backpack is already open");
			lock.unlock();
			return 4;
		}
		this.plugin.openBackpacks.put(uuid, new Date());
		
		Inventory inventory;
		try {
			inventory = this.getInventory(uuid, false);
		} catch (CannotLoadBackpackException e) {
			lock.unlock();
			throw e;
		}
		Bukkit.getPlayer(uuid).openInventory(inventory);
		this.log(Level.INFO, "Opened backpack of "+uuid.toString());
		lock.unlock();
		return 0;
	}
	
	/**
	 * Returns the allowed size of the backpack of the player
	 * 
	 * @param uuid UUID of the player
	 * @return Size of backpack, 0 if user is not allowed to use backpack
	 */
	public int backpackSize(UUID uuid){
		this.info("Get backpack size of "+uuid.toString());
		return PermissionsController.numberOfSlots(Bukkit.getPlayer(uuid));
	}
	
	/**
	 * 
	 * @param uuid UUID of player
	 * @return true if backpack is open<br>false if backpack is not open
	 */
	public boolean isOpen(UUID uuid){
		this.info("Get information if backpack of "+uuid.toString()+" is open");
		return this.plugin.openBackpacks.containsKey(uuid);
	}
	
	/**
	 * Add an ItemStack to the backpack of the player. If backpack is open at time, it is waited until backpack is closed.
	 * 
	 * @param uuid UUID of the player
	 * @param itemstack The ItemStack to be added
	 * @return A HashMap containing items that didn't fit
	 * @throws CannotLoadBackpackException Thrown if backpack cannot be loaded
	 */
	public ItemStack addItemStack(UUID uuid, ItemStack itemstack) throws CannotLoadBackpackException{
		lock.lock();
		this.info("Try to add ItemStack to backpack of "+uuid.toString());
		while(this.isOpen(uuid)){
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {}
		}
		this.plugin.openBackpacks.put(uuid, new Date());
		
		Inventory inv = null;
		try {
			inv = this.getInventory(uuid, false);
		} catch (CannotLoadBackpackException e) {
			lock.unlock();
			throw e;
		}
		
		HashMap<Integer, ItemStack> map = inv.addItem(itemstack);
		
		this.setInventory(uuid, inv, false);
		
		this.plugin.openBackpacks.remove(uuid);
		lock.unlock();
		if(map.isEmpty()){
			return null;
		}else{
			return (ItemStack) map.values().toArray()[0];
		}
	}
	
	/**
	 * Checks if backpack contains an itemstack
	 * 
	 * @param uuid UUID of the player
	 * @param itemstack ItemStack to test
	 * @return <code>true</code> backpack contains itemstack<br><code>false</code> backpack doesn't contains itemstack
	 * @throws CannotLoadBackpackException Thrown if backpack cannot be loaded
	 */
	public boolean containsItemStack(UUID uuid, ItemStack itemstack) throws CannotLoadBackpackException{
		Inventory inv = this.getInventory(uuid);
		return inv.contains(itemstack);
	}
	
	/**
	 * Checks if the normal inventory of an online player contains the backpack item
	 * 
	 * @param uuid UUID of the player
	 * @return <code>true</code> backpack contains backpack item<br><code>false</code> backpack doesn't contains backpack item or player is offline
	 */
	public boolean hasBackpackItem(UUID uuid){
		OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
		if(!player.hasPlayedBefore())
			return false;
		if(!player.isOnline())
			return false;
		
		return Backpack.containsBackpackItem(player.getPlayer().getInventory());
	}
	
	Map<UUID, String> getInventoryStrings(UUID uuid) throws IOException{
		this.lock.lock();
		Map<UUID, String> inventorys = new HashMap<UUID, String>();
		
		if(uuid!=null){
			inventorys.put(uuid, InventoryStringDeSerializer.InventoryToString(inventare.get(uuid)));
		}
		else
			for(Map.Entry<UUID, Inventory> entry : inventare.entrySet()){
				inventorys.put(entry.getKey(), InventoryStringDeSerializer.InventoryToString(entry.getValue()));
			}
		this.lock.unlock();
		return inventorys;
	}
	
}
