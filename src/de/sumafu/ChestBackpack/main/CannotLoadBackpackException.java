package de.sumafu.ChestBackpack.main;

import java.util.UUID;

@SuppressWarnings("serial")
public class CannotLoadBackpackException extends Exception {
	public CannotLoadBackpackException(UUID uuid){
		super("Cannot load backpack of player with uuid "+uuid.toString());
	}
	public CannotLoadBackpackException(UUID uuid, Exception original){
		super("Cannot load backpack of player with uuid "+uuid.toString(), original);
	}
}
