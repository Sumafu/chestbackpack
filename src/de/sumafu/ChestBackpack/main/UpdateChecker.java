package de.sumafu.ChestBackpack.main;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.json.JSONException;
import org.json.JSONObject;

public class UpdateChecker implements Runnable {
	public UpdateChecker(ChestBackpack plugin){
		this.plugin = plugin;
	}
	
	private ChestBackpack plugin = null;
	String noticeString = "";
	
	@Override
	public void run() {
		
		 try {
			URL url = new URL("http://sumafu.bitbucket.org/versions/ChestBackpack.json");
			URLConnection con = url.openConnection();
			con.setUseCaches(false);
			con.connect();
			 
			InputStream in = con.getInputStream();
			
			String jsonData = IOUtils.toString(in);
			
			JSONObject jo = new JSONObject(jsonData);
			JSONObject vs = jo.getJSONObject("version");
			
			if(!jo.getString("name").equals("ChestBackpack") ||
			   !jo.getString("author").equals("Sumafu") ||
			   !jo.getString("type").equals("Bukkitplugin") ||
			   !jo.has("path")){
				throw new JSONException("");
			}
			if(!vs.has("major") ||
			   !vs.has("minor") ||
			   !vs.has("patch") ||
			   !vs.has("date")){
				throw new JSONException("");
			}
			
			int nMajor = vs.getInt("major");
			int nMinor = vs.getInt("minor");
			int nPatch = vs.getInt("patch");
			
			String mVersion = this.plugin.getDescription().getVersion();
			String[] versions = mVersion.split("[.]");
			
			int oMajor = Integer.parseInt(versions[0]);
			int oMinor = Integer.parseInt(versions[1]);
			int oPatch = Integer.parseInt(versions[2]);
			
			if( (oMajor < nMajor) ||
				(oMajor==nMajor && oMinor < nMinor) ||
				(oMajor==nMajor && oMinor==nMinor && oPatch < nPatch)){
				
				String newVersion = nMajor+"."+nMinor+"."+nPatch;
				String date = vs.getString("date");
				String path = jo.getString("path");
				
				this.noticeString = "Your version of ChestBackpack is out of date. Latest version is v"+newVersion+", released on "+date+". ";
				this.noticeString+= "You can download latest version with this link: "+path;
			}
			
			
		} catch(JSONException e){
			this.noticeString = "Updatechecker cann't check for updates, because update file is invalid! Is this version outdated?";
		} catch (IOException e) {
			this.noticeString = "Updatechecker cann't check for updates, because update file is invalid! Is this version outdated?";
			e.printStackTrace();
		} finally {
			this.sendNotifications();
		}

	}
	
	private void sendNotifications(){
		if(this.noticeString.equals(""))return;
		this.plugin.getLogger().info(""+ChatColor.RED+ChatColor.BOLD+this.noticeString);
		
		for(Player player: Bukkit.getOnlinePlayers()){
			if(player.isOp()){
				player.sendMessage(""+ChatColor.RED+ChatColor.BOLD+this.noticeString);
			}
		}
	}

}
