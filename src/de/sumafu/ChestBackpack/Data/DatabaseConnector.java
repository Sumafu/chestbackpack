package de.sumafu.ChestBackpack.Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import de.sumafu.ChestBackpack.main.ChestBackpack;

public class DatabaseConnector {
	public DatabaseConnector(ChestBackpack plugin){
		this.plugin = plugin;
	}
	
	private ChestBackpack plugin = null;
	private Connection coc = null;
	
	boolean connectMySQL(){
		String host = plugin.getConfig().getString("mysql.host");
		String database = plugin.getConfig().getString("mysql.database");
		String username = plugin.getConfig().getString("mysql.username");
		String password = plugin.getConfig().getString("mysql.password");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			plugin.getLogger().log(Level.SEVERE, "No JDBC driver found!");
			e.printStackTrace();
			return false;
		}
		plugin.getLogger().info("MySQL JDBC Driver Registered!");
		plugin.getLogger().info("Try to connect with database server...");
		try {
			coc = DriverManager.getConnection("jdbc:mysql://"+host+"/"+database+"?autoReconnect=true", username, password);
	 
		} catch (SQLException e) {
			plugin.getLogger().log(Level.SEVERE, "Connection Failed! Check output console");
			e.printStackTrace();
			return false;
		}
		if (coc != null) {
			plugin.getLogger().info("Successfully connected to MySQL");
		} else {
			plugin.getLogger().log(Level.SEVERE, "Failed to initiate connection!");
			return false;
		}
		
		try {
			if(!this.checkTable()){
				plugin.getLogger().log(Level.SEVERE, "MySQL-database is not valid");
				return false;
			}
		} catch (SQLException e) {
			plugin.getLogger().log(Level.SEVERE, "Failed to check the database! Check output console");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean checkTable() throws SQLException{
		Statement stmt = coc.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS Anzahl FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'de.sumafu.ChestBackpack.Backpack'");
		rs.next();
		if(rs.getInt("Anzahl")==0){
			String create_table_sql="CREATE TABLE IF NOT EXISTS `de.sumafu.ChestBackpack.Backpack` ("
									+" `BpID` int(11) NOT NULL,"
									+" `BpUUID` text NOT NULL,"
									+" `BpInvString` text NOT NULL"
									+" ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
			stmt.execute(create_table_sql);
			
			create_table_sql="ALTER TABLE `de.sumafu.ChestBackpack.Backpack` ADD PRIMARY KEY (`BpID`)";
			stmt.execute(create_table_sql);
			
			create_table_sql="ALTER TABLE `de.sumafu.ChestBackpack.Backpack` MODIFY `BpID` int(11) NOT NULL AUTO_INCREMENT";
			stmt.execute(create_table_sql);
			
			plugin.getLogger().info("New MySQL table created");
			return true;
		}
		
		String get_table_colums_sql = "SHOW COLUMNS FROM `de.sumafu.ChestBackpack.Backpack`";
		ResultSet rs2 = stmt.executeQuery(get_table_colums_sql);
		List<String> colums = new ArrayList<String>();
		while(rs2.next()){
			colums.add(rs2.getString("Field"));
		}
		if(colums.size()==3
				&& colums.contains("BpID")
				&& colums.contains("BpUUID")
				&& colums.contains("BpInvString")){
			return true;
		}
		
		return false;
	}
	
	public Connection getConnection(){
		return this.coc;
	}
	
	void closeConnection(){
		try {
			this.coc.close();
		} catch (SQLException e) {
			this.plugin.getLogger().log(Level.SEVERE, "Failed to close database connetion correctly");
		}
	}
	
	boolean reActivateMySQLConnection(){
		try {
			this.coc.createStatement().execute("SELECT COUNT(*) FROM `de.sumafu.ChestBackpack.Backpack`");
			return true;
		} catch (SQLException e) {
			plugin.getLogger().info("Connection timed out, try to reconnect first time...");
		}
		try {
			this.coc.createStatement().execute("SELECT COUNT(*) FROM `de.sumafu.ChestBackpack.Backpack`");
			return true;
		} catch (SQLException e) {
			plugin.getLogger().info("Connection timed out, try to reconnect second time...");
		}
		try {
			this.coc.createStatement().execute("SELECT COUNT(*) FROM `de.sumafu.ChestBackpack.Backpack`");
			return true;
		} catch (SQLException e) {
			plugin.getLogger().info("Connection timed out, try to reconnect third time...");
		}
		try {
			this.coc.createStatement().execute("SELECT COUNT(*) FROM `de.sumafu.ChestBackpack.Backpack`");
			return true;
		} catch (SQLException e) {
			plugin.getLogger().info("Connection timed out, could not reconnect, show server log for error message");
			e.printStackTrace();
			return false;
		}
	}
}
