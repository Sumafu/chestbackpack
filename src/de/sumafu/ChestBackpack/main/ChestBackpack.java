package de.sumafu.ChestBackpack.main;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.WorldSaveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import de.sumafu.ChestBackpack.Data.DataHandler;
import de.sumafu.ChestBackpack.Tools.Localize;
import de.sumafu.ChestBackpack.Tools.MyLogger;

public class ChestBackpack extends JavaPlugin implements Listener {
	
	//public Map<UUID, String> inventare = new HashMap<UUID, String>();
	public Map<UUID, Boolean> warmup = new HashMap<UUID, Boolean>();
	public InventoryController invControl = null;
	public Localize localStrings = null;
	public DataHandler dataHandler = null;
	private MyLogger logger = null;
	private UpdateChecker updateChecker = null;
	Map<UUID, Date> openBackpacks = new HashMap<UUID, Date>();
	
	private static ChestBackpackAPI api = null;
	
	public boolean MigrationMode = false;
	
	public void onEnable(){
		this.localStrings = new Localize(this);
		
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
		
		String langFile = this.getConfig().getString("language_file");
		this.localStrings.setLanguageFile(langFile);
		
		switch(this.getConfig().getString("datamode")){
		case "mysql":
			this.getLogger().info("Use MySQL database");
			break;
		case "flatfile":
			this.getLogger().info("Use flatfile as database");
			break;
		default:
			this.getLogger().log(Level.WARNING, "No valid data mode found in config.yml, using flatfile now");
		}
		
		this.dataHandler = new DataHandler(this);
		if(this.getConfig().getString("datamode")=="mysql" && !this.dataHandler.getMySQLReady()){
			this.getServer().getPluginManager().disablePlugin(this);
			return;
		}
		
		try {
			this.logger = new MyLogger(this);
		} catch (IOException e) {
			e.printStackTrace();
			this.getServer().getPluginManager().disablePlugin(this);
		}
		
		this.invControl = new InventoryController(this);
		
		ChestBackpack.api = new ChestBackpackAPI(this);
		
		this.getServer().getPluginManager().registerEvents(this, this);
		this.getServer().getPluginManager().registerEvents(this.invControl, this);
		
		updateChecker = new UpdateChecker(this);
		this.getServer().getScheduler().runTaskAsynchronously(this, updateChecker);
		
	}
	
	MyLogger getMyLogger(){
		return this.logger;
	}
	
	public void onDisable(){
		this.getLogger().info("Flush data to database");
		
		this.invControl.flusher.data.put("onlyUUID", null);
		this.invControl.flusher.data.put("onlyUuidInventory", null);
		
		this.invControl.flusher.run();
		
		try {
			this.logger.closeLogFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	Date lastMigrateInquiry = null;
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if(cmd.getName().equalsIgnoreCase("backpack")){
				this.getServer().getScheduler().runTaskAsynchronously(this, new CommandBackpack(this, sender, args));
			return true;
		}
		return false; 
	}
	
	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent event){
		if(this.MigrationMode){
			event.getPlayer().kickPlayer(this.localStrings.getLocalizedString("other.migrateKick"));
			return;
		}
		
		if(!ChestBackpack.getAPI().intContainsKey(event.getPlayer().getUniqueId())){
			int warmup = this.getConfig().getInt("backpackWarmUp");
			
			this.invControl.loadData(event.getPlayer().getUniqueId(), warmup);
			
		}
		
		if(event.getPlayer().isOp() && !this.updateChecker.noticeString.equals("")){
			event.getPlayer().sendMessage(""+ChatColor.RED+ChatColor.BOLD+this.updateChecker.noticeString);
		}
		
		
		
	}
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event){
		if(this.MigrationMode) return;
		
		this.getLogger().info(event.getPlayer().getName()+" has disconnected");
		
		this.invControl.flushData(event.getPlayer().getUniqueId());
		
		ChestBackpack.getAPI().intRemoveInventory(event.getPlayer().getUniqueId());
	}
	
	
	@EventHandler
	public void onWorldSave( WorldSaveEvent event ){
		this.invControl.flushData(null);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event){
		if(!PermissionsController.dropInventoryThroughDeath(event.getEntity()))return;
		
		if(Backpack.containsBackpackItem(event.getEntity().getInventory()))this.invControl.dropAllItems(event.getEntity());
	}
	
	public static ChestBackpackAPI getAPI(){
		return ChestBackpack.api;
	}
	
	// Open Backpack
	void openBackpack(Player player){
		UUID uuid = player.getUniqueId();
		try {
			int result = ChestBackpack.getAPI().openBackpack(uuid);
			
			switch(result){
			case 0:
				this.getMyLogger().info(player.getName()+" opened backpack");
				break;
			case 1:
				// Offline
				break;
			case 2:
				player.sendMessage(ChatColor.RED+this.localStrings.getLocalizedString("error.notAllowed"));
				break;
			case 3:
				player.sendMessage(ChatColor.RED+this.localStrings.getLocalizedString("error.cannotOpenWarmup"));
				break;
			case 4:
				// Open
				break;
			}
		} catch (CannotLoadBackpackException e) {
			player.sendMessage(ChatColor.RED+this.localStrings.getLocalizedString("error.errorOccured"));
			this.getMyLogger().log(Level.SEVERE, "Failed to open backpack of "+player.getName()+". Show server log for detailed error.");
			e.printStackTrace();
		}
	}
	
	// Some Wrappers for classes in other packages
	public void clearInventorys(){
		ChestBackpack.getAPI().intClear();
	}
	public void setInventory(UUID uuid, Inventory inv){
		ChestBackpack.getAPI().setInventory(uuid, inv);
	}
	public int getInventorySize(UUID uuid){
		return PermissionsController.numberOfSlots(Bukkit.getPlayer(uuid));
	}
}
