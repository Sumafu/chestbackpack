package de.sumafu.ChestBackpack.main;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyRunnable implements Runnable {
	public MyRunnable(Object... params){
		this.params = params;
	}
	Object[] params = null;
	final Lock lock = new ReentrantLock();
	public Map<String, Object> data = new HashMap<String, Object>();
	
	@Override
	public void run() {}

}